package fr.ulille.iut.pizzaland.dto;
import java.util.ArrayList;
import java.util.UUID;

import fr.ulille.iut.pizzaland.beans.Ingredient;

public class PizzaDto {
    private UUID id;
    private String name;
    private ArrayList<UUID> ingredients;

    public PizzaDto() {
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getName() {
      return name;
    }

    public void setIngredients(ArrayList<UUID> ingredients) {
    	this.ingredients = ingredients;
    }

    public ArrayList<UUID> getIngredients() {
    	return ingredients;
    }
}

